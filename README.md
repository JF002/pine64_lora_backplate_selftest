# Pine64 LoRa backplate selftest
This is a simple test program for the LoRa backplate for the PinePhone. It checks that the LoRa backplate is correctly installed and that the bridge (I²C <-> SPI) MCU is flashed with [a working firmware](https://github.com/zschroeder6212/tiny-i2c-spi/tree/master/src).

The test consists in writing a few bytes in the "payload" buffer of the SX1262 and to read it back. The consistency of the data is checked to validate the test.

# Usage
Simply run this application on a PinePhone equipped with the LoRa backplate **as root (or using sudo)**:
```bash
sudo ./pine64_lora_backplate_selftest
```

This is the output of a **successful** test:
```bash
$ sudo ./pine64_lora_backplate_selftest
PinePhone LoRa backplate selftest application
[*] Opening the I²C port
[*] Configure I²C device address
[*] Sending the "WriteBuffer" command to the SX1262
        WriteBuffer response : 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 
[*] Sending the "ReadBuffer" command to the SX1262
        ReadBuffer response : 0xa2 0xa2 0xa2 0x10 0x20 0x30 0x40 0x50 0xaa 0x55 0x 0 0xff 
[*] Checking data validity

==> TEST SUCCESFUL!
```

This is the output if the **data validity check fails**:
```bash
PinePhone LoRa backplate selftest application
[*] Opening the I²C port
[*] Configure I²C device address
[*] Sending the "WriteBuffer" command to the SX1262
        WriteBuffer response : 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 0xa2 
[*] Sending the "ReadBuffer" command to the SX1262
        ReadBuffer response : 0xa2 0xa2 0xa2 0x10 0x20 0x30 0x40 0x50 0xaa 0x55 0x 0 0xff 
[*] Checking data validity
        [!] Invalid data at position 0. Received : 0x10 - Expected: 0x10
        [!] Invalid data at position 1. Received : 0x20 - Expected: 0x20
        [!] Invalid data at position 2. Received : 0x30 - Expected: 0x30
        [!] Invalid data at position 3. Received : 0x40 - Expected: 0x40
        [!] Invalid data at position 4. Received : 0x50 - Expected: 0x50
        [!] Invalid data at position 5. Received : 0xaa - Expected: 0xaa
        [!] Invalid data at position 6. Received : 0x55 - Expected: 0x55
        [!] Invalid data at position 7. Received : 0x 0 - Expected: 0x 0
        [!] Invalid data at position 8. Received : 0xff - Expected: 0xff

==> TEST FAILED!
```

This is the output if **the LoRa backplate is not installed** or if **the bridge MCU is not working properly**:
```bash
$ sudo /tmp/tmp.vjTJsCKClN/cmake-build-debug-pinephone/pine64_lora_backplate_selftest
PinePhone LoRa backplate selftest application
[*] Opening the I²C port
[*] Configure I²C device address
[*] Sending the "WriteBuffer" command to the SX1262
Failed to write to the i2c bus: No such device or address
```

# Build
Build this project as any CMake project:
```bash
git clone xxx
cd pine64_lora_backplate_selftest
mkdir build && cd build
cmake ../
make
```

Result:
```bash
$ cmake ../
-- The C compiler identification is GNU 10.2.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/alarm/pine64_lora_backplate_selftest/build

```