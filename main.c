#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint-gcc.h>
#include <stdlib.h>

int OpenPort(const char* fileName) {
  int f = open(fileName, O_RDWR);
  if(f < 0) {
    perror("Failed to open the i2c bus");
  }
  return f;
}

int ConfigureDeviceAddress(int i2c, const int addr) {
  if (ioctl(i2c, I2C_SLAVE, addr) < 0) {
    perror("Failed to acquire bus access and/or talk to slave.\n");
    return -1;
  }
  return 0;
}

int I2CWrite(int i2c, const uint8_t data[], const size_t size) {
  if(write(i2c, data, size) != size) {
    perror("Failed to write to the i2c bus");
    return -1;
  }
  return 0;
}

int I2CRead(int i2c, uint8_t data[], const size_t size) {
  /* For some reason, the bridge MCU only supports reading 1 byte at a time */
  for(int i = 0; i < size; i++) {
    if (read(i2c, &data[i], 1) != 1) {
      perror("Failed to read from the i2c bus.");
      return -1;
    }
    usleep(10000);
  }
  return 0;
}

void Display(const char* name, const uint8_t data[], size_t size) {
  printf("%s : ", name);
  for(int i = 0; i < size; i++) {
    printf("0x%2x ", data[i]);
  }
  printf("\n");
}

int main() {
  int i2cFile;
  const char *i2cFileName = "/dev/i2c-2";
  const int deviceAddress = 0x28;

  printf("PinePhone LoRa backplate selftest application\n");

  printf("[*] Opening the I²C port\n");
  i2cFile = OpenPort(i2cFileName);
  if(i2cFile < 0) {
    exit(-1);
  }

  printf("[*] Configure I²C device address\n");
  if(ConfigureDeviceAddress(i2cFile, deviceAddress) < 0) {
    exit(-1);
  }

  /* Send the command "WriteBuffer" to the SX1262 with a dummy data buffer */
  /* Buffer content:
   *  [0] = 0x01 : "TRANSMIT" command for the bridge MCU (see https://github.com/zschroeder6212/tiny-i2c-spi).
   *  [1] = 0x0E : "WriteBuffer" command for the SX1262 (see the SX1262 Datasheet rev1.2 chapter 13.2.3 "WriteBuffer function")
   *  [2] = 0x00 : Offset for the WriteBuffer command
   *  [3..11] : buffer content
   *  */
  printf("[*] Sending the \"WriteBuffer\" command to the SX1262\n");
  uint8_t cmd1[] = {0x01, 0x0E, 0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0xAA, 0x55, 0x00, 0xFF};
  if(I2CWrite(i2cFile, cmd1, 12) < 0) {
    exit(-1);
  }

  usleep(10000);
  uint8_t response1[11];
  if(I2CRead(i2cFile, response1, 11) < 0) {
    exit(-1);
  }
  Display("\tWriteBuffer response", response1, 11);

  /* Send the command "ReadBuffer" to the SX1262 */
  /* Buffer content:
   *  [0] = 0x01 : "TRANSMIT" command for the bridge MCU (see https://github.com/zschroeder6212/tiny-i2c-spi).
   *  [1] = 0x1E : "ReadBuffer" command for the SX1262 (see the SX1262 Datasheet rev1.2 chapter 13.2.4 "ReadBuffer function")
   *  [2] = 0x00 : Offset for the WriteBuffer command
   *  [3] = 0x00 : NOP
   *  [4..12] : buffer content
   *  */
  printf("[*] Sending the \"ReadBuffer\" command to the SX1262\n");
  uint8_t cmd2[] = {0x01, 0x1E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  usleep(10000);
  if(I2CWrite(i2cFile, cmd2, 13) < 0) {
    exit(-1);
  }
  usleep(10000);
  uint8_t response2[12];
  if(I2CRead(i2cFile, response2, 12) < 0) {
    exit(-1);
  }
  Display("\tReadBuffer response", response2, 12);

  printf("[*] Checking data validity\n");
  size_t errorCount = 0;
  for(int i = 0; i < 9; i++) {
    if(response2[i + 3] != cmd1[i + 3]) {
      printf("\t[!] Invalid data at position %d. Received : 0x%2x - Expected: 0x%2x\n", i, response2[i + 3], cmd1[i + 3]);
      errorCount++;
    }
  }

  if(errorCount > 0) {
    printf("\n==> TEST FAILED!\n");
    return -1;
  } else {
    printf("\n==> TEST SUCCESFUL!\n");
    return 0;
  }
}